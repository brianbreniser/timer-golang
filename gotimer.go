package main

import (
	"fmt"
	"log"
	"os"
	"time"
    "strconv"
    "github.com/gen2brain/beeep"
)

func main() {
    seconds, message := getArgs()

    fmt.Printf("Starting timer for %d seconds\n", seconds)
    fmt.Printf("%d seconds remaining\n", seconds)

    for seconds > 0 {
        time.Sleep(1 * time.Second)
        seconds -= 1

        fmt.Print("\033[1A") // Move cursor up one line
        fmt.Print("\033[2K") // Erase the entire current line
        fmt.Printf("%d seconds remaining\n", seconds)
    }

    endMessage := "Your timer is up with the message: " + message
    beeep.Notify("Timer is up", endMessage, "")

    fmt.Println(endMessage)
}

func getArgs() (int64, string) {
    allArgs := os.Args[1:]
    if len(allArgs) < 2 {
        dumpHelp()
        log.Fatal("Not enough arguments")
    }

    message := ""
    for _, arg := range allArgs[1:] {
        message += arg + " "
    }

    seconds := parseTimeAsSeconds(allArgs[0])

    return seconds, message
}

func parseTimeAsSeconds(arg string) int64 {
    minuteString := arg
    minutesFloat, err := strconv.ParseFloat(minuteString, 64)
    if err != nil {
        dumpHelp()
        log.Panicf("ParseFloat failed with error: %s", err)
    }

    seconds := int64(minutesFloat * 60)

    return seconds
}

func dumpHelp() {
    fmt.Println("Usage: ")
    fmt.Println("gotimer <number> <message>")
    fmt.Println("\t<number> is the number of minutes to wait before printing the message")
    fmt.Println("\t<message> is a space separated string of words that will be printed after the timer is up")
    fmt.Println("Example: ")
    fmt.Println("\tgotimer 5 Time to get back to work # Waits for 5 minutes then tells you to get back to work")
}

